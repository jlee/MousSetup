/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******	Settings.c ********************************************************\

Project:	Ursula (RISC OS for Risc PC II)
Component:	Mouse configuration plug-in
This file:	GUI <-> settings routines

History:
Date		Who	Change
----------------------------------------------------------------------------
09/12/1997	BJGA	Split from Main
			Added these headers
15/01/1998	BJGA	Adapted to use stored CMOS defaults
11/05/1998	BJGA	Adapted for use with new Wimp CMOS usage
			Activated autofronting on/off and time options
			Added autoscroll pause time support

\**************************************************************************/

/* CLib */
#include <string.h>
#include "swis.h"
#include "Global/OsWords.h"
/* Toolbox */
#include "toolbox.h"
#include "window.h"
#include "gadgets.h"
/* Common */
#include "cmos.h"
#include "misc.h"
/* local headers */
#include "Main.h"
#include "MouseType.h"
#include "Settings.h"  /* includes prototypes for this file */

#define mousespeed_slider_upperbound	((int) 9)

const cmos cmos_details [18] = { { 0xC2, 0, 8 },	/* MouseMultiplier */
				 { 0x1D, 0, 8 },	/* MouseType */

				 { 0xDD, 0, 4 },	/* (WimpDragDelay EOR 5) */
				 { 0xDE, 0, 1 },	/* WimpDragDelayUnit */

				 { 0xDE, 2, 5 },	/* (WimpDragMove/4 EOR 8) */

				 { 0xDF, 0, 4 },	/* (WimpDoubleClickDelay EOR 10) */
				 { 0x16, 0, 1 },	/* WimpDoubleClickDelayUnit */

				 { 0x16, 2, 5 },	/* (WimpDoubleClickMove/4 EOR 8) */

				 { 0xDD, 4, 4 },	/* (WimpAutoScrollDelay EOR 5) */
				 { 0xDE, 1, 1 } };	/* WimpAutoScrollDelayUnit */

/******	settings_read() ***************************************************\

Purpose:	Reads current / default settings, reflect them in GUI
In:		Routine to determine settings (cmos_read or cmos_default)

\**************************************************************************/

void settings_read (int(*get)(cmos item, void *messages))
{
  int	value;
//
  value = get (MouseMultiplier, &messages);
  if (value >= 0x80) value = 0x100-value; /* force positive */
  value = max(value, 1);                  /* force in range */
  value = min(value, mousespeed_slider_upperbound);
  throw (slider_set_value (0, mainwindow_id, mainwindow_mousespeed, value));
//
  throw (stringset_set_selected (0, mainwindow_id, mainwindow_mousetype, mousetype_get (get)));
//
  value = get (WimpDragDelay, &messages) ^ 5;
  if (1 == get (WimpDragDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_dragdelay, value));
//
  value = (get (WimpDragMove, &messages) ^ 8) * 4;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_dragdist, value));
//
  value = get (WimpDoubleClickDelay, &messages) ^ 10;
  if (1 == get (WimpDoubleClickDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_dclickdelay, value));
//
  value = (get (WimpDoubleClickMove, &messages) ^ 8) * 4;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_dclickdist, value));
//
  value = get (WimpAutoScrollDelay, &messages) ^ 5;
  if (1 == get (WimpAutoScrollDelayUnit, &messages)) value *= 10;
  throw (numberrange_set_value (0, mainwindow_id, mainwindow_autoscr, value));
//
}

/******	settings_write() **************************************************\

Purpose:	Reads GUI, reflect in current and configured settings
Out:		TRUE => operation performed successfully
		  (if user backs out of changing the mouse driver,
		  no other settings are made either)

\**************************************************************************/

BOOL settings_write (void)
{
  int value;
  char string [256];
  unsigned char step [3];
//
  throw (stringset_get_selected (0, mainwindow_id, mainwindow_mousetype, string, 256, NULL));
  if (mousetype_set (string) == 2) return FALSE;
//
  throw (slider_get_value (0, mainwindow_id, mainwindow_mousespeed, &value));
  cmos_write (MouseMultiplier, value);
  step[0] = OWPointerAndMouse_SetMouseFactors;
  step[1] = (unsigned char) value;
  step[2] = (unsigned char) value;
  _swi (OS_Word, _INR(0,1), OsWord_DefinePointerAndMouse, step);
//
  throw (numberrange_get_value (0, mainwindow_id, mainwindow_dragdelay, &value));
  sprintf (string, "Configure WimpDragDelay %i", value);
  _swi (OS_CLI, _IN(0), string);
//
  throw (numberrange_get_value (0, mainwindow_id, mainwindow_dragdist, &value));
  sprintf (string, "Configure WimpDragMove %i", value);
  _swi (OS_CLI, _IN(0), string);
//
  throw (numberrange_get_value (0, mainwindow_id, mainwindow_dclickdelay, &value));
  sprintf (string, "Configure WimpDoubleClickDelay %i", value);
  _swi (OS_CLI, _IN(0), string);
//
  throw (numberrange_get_value (0, mainwindow_id, mainwindow_dclickdist, &value));
  sprintf (string, "Configure WimpDoubleClickMove %i", value);
  _swi (OS_CLI, _IN(0), string);
//
  throw (numberrange_get_value (0, mainwindow_id, mainwindow_autoscr, &value));
  sprintf (string, "Configure WimpAutoScrollDelay %i", value);
  _swi (OS_CLI, _IN(0), string);
//
  return TRUE;
}
